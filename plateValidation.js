

NO_VALID_PLATE = "NO VALID PLATE WITH THOSE CHARACTERS";
VALID_PLATE = "VALID PLATE";

function isValidPlate(plate) {

  var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/i;
  type="";

  if(plate.match(re) != null) {
    type=VALID_PLATE;
  } else {
    type= NO_VALID_PLATE;
  }
  return type;
}